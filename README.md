﻿﻿﻿# MisterGames BlueprintLib v0.1.0

## Features
- todo

## Assembly definitions
- MisterGames.BlueprintLib

## Dependencies
- MisterGames.Common
- MisterGames.Blueprints
- MisterGames.Scenario
- MisterGames.Input
- MisterGames.Scenes

## Installation 
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common) package
- Top menu MisterGames -> Packages, add packages: 
  - [Blueprints](https://gitlab.com/theverymistergames/blueprints/)
  - [Input](https://gitlab.com/theverymistergames/input/)
  - [Scenario](https://gitlab.com/theverymistergames/scenario/)
  - [Scenes](https://gitlab.com/theverymistergames/scenes/)
  - [BlueprintLib](https://gitlab.com/theverymistergames/blueprintlib/)